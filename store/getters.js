export default {
  allUsers(state) {
    return state.usersState;
  },
  viewAllDept(state) {
    return state.allDepartments;
  },
  viewPositionsByDept(state) {
    return state.positionsByDept;
  },
  getAccessRights(state) {
    // console.log(state.accessRightsState, "00");
    return state.accessRightsState;
  },
  getActivityLogs(state) {
    return state.activityLogsState;
  },
  getFilteredDept(state) {
    return state.filterDepartment;
  },
  getKeptForRef_Reports(state) {
    return state.keptForRef_Reports;
  }
};

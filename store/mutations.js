import state from "./state";
export default {
  LOGIN_USER(state, user) {
    // console.log(user);
    state.usersState.push(user);
  },
  USER_NAME(state, user) {
    state.userInfo = user;
    // console.log(user);
  },

  openPage(state, data) {
    state.activeN = data[0].id;
    state.activeS = data[0].subID;
    // console.log("=============================");
    // console.log("openPage()");
    // console.log("=============================");
    // console.log("data[0].subID: " + data[0].subID);
    // console.log("data[0].colID: " + data[0].colID)
    // console.log("data[0].route: " + data[0].route);
    // console.log("=============================");
    // console.log("open: " + data[0].open);
    // console.log("=============================");
    if (data[0].open == true) {
      if (data[0].colID == localStorage.activeC) {
        state.collapseID = "collpase-repeat";
      } else {
        state.collapseID = data[0].colID;
      }
    } else if (data[0].open == false) {
      state.activeN = data[0].id;
      state.activeS = data[0].subID;
      // console.log(data[0].colID + " - " + localStorage.activeC)
      state.collapseID = data[0].colID;
      if (localStorage.activeC == data[0].colID) {
        state.collapseID = data[0].colID;
      }
    }

    localStorage.activeN = data[0].id;
    localStorage.activeS = data[0].subID;
    localStorage.activeC = data[0].colID;
  },
  viewDepartments(state, data) {
    // console.log("============================");
    // console.log("Mutation: View All Departments");
    // console.log("============================");
    // console.log(data);

    // INSERT SELECTED DEPARTMENT
    state.allDepartments = data;

    // CLEAR TO INSERT NEW POSITIONS FROM SELECTED DEPARTMENT
    state.positionsByDept = [];
  },
  viewPositions(state, data) {
    state.positionsByDept = data.view;
    // console.log(state.positionsByDept);
  },
  Access_Rights(state, access) {
    state.accessRightsState = access;
  },
  Activity_Logs(state, logs) {
    // console.log("user");
    state.activityLogsState = logs;
    // console.log(logs);
  },
  statusServe(state, data) {
    state.serverStatus = data;
  },
  setPerPage(state, data) {
    state.perPage = data;
    localStorage.totalPage = data;
    // console.log("set perPage: " + data);
    // console.log(localStorage.totalPage);
  },
  setUserData(state, data) {
    state.userData = data;
  },
  setUserDataStatus(state, data) {
    state.userDataStatus = data;
  },
  filteredLogs(state, data) {
    if (data.length < 1) {
      state.filteredUserLogs = [];
    } else {
      state.filteredUserLogs = data;
    }
  },
  allLogs(state, data) {
    if (data.length < 1) {
      state.allUserLogs = [];
    } else {
      state.allUserLogs = data;
    }
  },
  viewUserUsageData(state, data) {
    if (data.length < 1) {
      state.userUsageData = [];
    } else {
      state.userUsageData = data;
    }
  },
  allUserUsageData(state, data) {
    if (data.length < 1) {
      state.allUserUsageData = [];
    } else {
      state.allUserUsageData = data;
    }
  },
  viewApplicantInfo(state, data) {
    if (data == "Failed to fetch applicant information.") {
      state.applicantDetails = { noData: true, mess: data };
    } else if (typeof data === "string") {
      state.applicantDetails = { noData: true, mess: data };
    } else {
      console.log(data.message);

      // STORE APPLICANT INFO IN state.applicantDetails
      state.applicantDetails = data.applicantInformation[0];
      // console.log(state.applicantDetails);

      // STORE EXAM RESULTS IN state.examResults
      state.examResults = data.applicantInformation[0].onlineExamResult;
      // console.log(data.applicantInformation[0].onlineExamResult);

      // STORE IQBE RESULTS IN state.iqbeResults
      state.iqbeResults = data.applicantInformation[0].iqbeResult;
      // console.log(data.applicantInformation[0].iqbeResult);

      // STORE ASSESSMENT RESULTS IN state.assessmentResults
      state.assessmentResults =
        data.applicantInformation[0].assessmentInformation;
      // console.log(data.applicantInformation[0].assessmentInformation);
    }
  },
  mob_select_time(state, data) {
    // console.log(data);
    state.mob_selected_time = data.thisTime;
    state.mob_selected_time_open = data.open;
  },
  mob_select_date(state, data) {
    state.mob_selected_date = data.date;
    state.mob_selected_date_open = data.open;
  },
  mob_open_modal(state, data) {
    state.mob_modal = data.act_name;
    state.mob_applicant = data.applicant;
  },
  mob_hide_actions(state, data) {
    state.hide_actions = data;
  },
  mob_hide_header_btns(state, data) {
    state.hide_header_btns = data;
  },
  mob_set_active_tab(state, data) {
    state.mob_active_tab = data;
  },
  mob_set_selected_appl_stage(state, data) {
    state.mob_appl_stage = data;
  },
  examRating(state, data) {
    state.rateExam = data;
  },
  iqbeRating(state, data) {
    state.rateIQBE = data;
  },
  viewReferralFormInfo(state, data) {
    state.referralForm = data;
    console.log(data);
  },
  enterEmployeeID(state, data) {
    state.enterEmployeeID = data;
  },
  viewQuestionInfo(state, data) {
    state.questionDetails = data;
  },
  addApplicant(state, data) {
    state.closeAddApplicant = data;
  },
  closeUserUsageData(state, data) {
    state.closeUserUsageDataView = data;
    state.userUsageData = [];
  },
  closeViewApplicants(state, data) {
    state.closeViewApplicant = data;
    state.applicantDetails = [];
    // console.log(data);
  },
  closeExamRating(state, data) {
    state.closeExamRating = data;
    state.rateExam = [];
  },
  closeIQBERating(state, data) {
    state.closeIQBERating = data;
    state.rateIQBE = [];
  },
  closeQuestionDetails(state, data) {
    state.closeQuestionInfo = data;
    state.questionDetails = [];
  },
  closeReferralForm(state, data) {
    state.closeReferralForm = data;
    state.referralForm = [];
  },
  closeEnterEmployeeID(state, data) {
    state.closeEnterEmployeeID = data;
    state.enterEmployeeID = [];
  },
  closeAddApplicant(state, data) {
    state.closeAddApplicant = data;
  },
  doneLoad(state, count) {
    state.doneLoading = parseInt(state.doneLoading) + parseInt(count);
  },
  resetLoader(state) {
    state.doneLoading = 0;
  },
  viewDetailsImgs(state, data) {
    console.log("check", data);
    state.detailsImgs = data;
  },
  FILTER_DEPT(state, data) {
    state.filterDepartment = data;
  },
  KEPTFORREF_REPORTS(state, data) {
    state.keptForRef_Reports = data;
  }
};

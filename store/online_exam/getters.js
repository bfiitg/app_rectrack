export default {
  allCategories(state) {
    return state.categoryState;
  },
  allQuestionTypes(state) {
    return state.questionTypesState;
  },
  allQuestions(state) {
    return state.questionsState;
  }
};

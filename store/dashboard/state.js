export default () => ({
  appFormData: [],
  applicantDetails: [],
  positionList: [],
  applicantStage: "",
  doneEndorseToDep: "",
  doneKeep: "",
  doneBlacklist: "",
  doneReject: "",
  doneOnline: "",
  doneEndorseForAssess: "",
  doneRetake: "",
  doneCreateSched: "",
  doneUpdate: "",
  doneShortlist: "",
  doneJobOffer: "",
  doneDeploy: "",
  doneChangeDeployDate: "",
  doneRate: "",
  doneReferral: "",
  doneReturnApplicant: "",
  allReceive: []
});

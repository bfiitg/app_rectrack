import axios from "axios";

export default {
  //Fetch All Department
  async getDeptAction({ commit }, { token }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/departments/admin/view`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => {
        commit("VIEW_DEPT", res.data);
        return res.data;
      })
      .catch(err => console.log(err));
  },
  //Action for adding new Department
  async addDeptAction({ commit }, { name, description, user_id, token }) {
    return await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/departments/admin/add`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        name,
        description,
        user_id
      }
    }).then(res => {
      commit("ADD_DEPT", res);
      return res;
    });
  },
  //Action for updating Department detail
  async editDeptAction({ commit }, { token, id, name, description, status }) {
    return await axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/departments/admin/edit/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        name,
        description,
        status
      }
    }).then(res => {
      commit("EDIT_DEPT", res);
      return res;
    });
  },
  //FETCH ALL TEAMS
  async getTeamAction({ commit }, { token }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/teams/admin/view`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => {
        commit("VIEW_TEAM", res.data);
        console.log(res.data);
        return res.data;
      })
      .catch(err => console.log(err));
  },

  //view TEAM DETAILS
  async viewTeamAction({ commit }, { token, id }) {
    // FETCH TEAM BY ID (WITH DEPARTMENT ID AND NAME)
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/teams/admin/view/${id}`,
      headers: { Authorization: `Bearer ${token}` }
    }).then(res => {
      console.log("FETCH TEAM: SUCCESS", res);
      console.log("=======================================");
      return res.data[Object.keys(res.data)[0]];
      // commit("viewApplicantInfo", res.data.response);
    });
  },
  //UPDATE TEAM DETAILS
  async editTeamAction(
    { commit },
    { token, id, name, description, department_id, no_of_items, status }
  ) {
    return await axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/teams/admin/edit/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        name,
        description,
        no_of_items,
        department_id,
        status
      }
    }).then(res => {
      commit("EDIT_TEAM", res);
      return res;
    });
  },
  //FETCH POSITION
  async getPositionAction({ commit }, { token }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/positions/view`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => {
        commit("VIEW_POSITION", res.data);
        return res.data;
      })
      .catch(err => console.log(err));
  },
  // ADD NEW POSITION
  async addPositionAction(
    { commit },
    { name, description, teams_id, token, is_office_staff }
  ) {
    return await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/positions/add`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        name,
        description,
        teams_id,
        is_office_staff
      }
    }).then(res => {
      commit("ADD_POS", res);
      return res;
    });
  },
  //UPDATE POSITION DETAILS
  async editPosAction(
    { commit },
    {
      token,
      name,
      description,
      teams_id,
      is_office_staff,
      is_vacant,
      status,
      id
    }
  ) {
    return await axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/positions/update/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        name,
        description,
        is_office_staff,
        teams_id,
        is_vacant,
        status
      }
    }).then(res => {
      commit("EDIT_POS", res);
      return res;
    });
  },
  //fetch system users main table
  async getUsersAction({ commit }, { token }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/users/admin/view`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => {
        commit("VIEW_SYSUSER", res.data.view);
        return res.data.view;
      })
      .catch(err => console.log(err));
  },

  // ADD NEW system user
  async addSysUserAction(
    { commit },
    {
      email,
      password,
      firstname,
      lastname,
      middlename,
      mobile,
      role_id,
      position_id,
      link,
      token
    }
  ) {
    return await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/users/admin/add`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        email,
        password,
        firstname,
        lastname,
        middlename,
        mobile,
        role_id,
        position_id,
        link
      }
    }).then(res => {
      commit("ADD_SYSUSER", res);
      return res;
    });
  },
  // fetch user data to edit system user details
  async getEditSysUserDetails({ commit }, { token, id }) {
    console.log(id);
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/users/admin/view/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    }).then(res => {
      console.log(res);
      commit("VIEW_SYSUSERDET", res);

      return res;
    });
  },
  //edit system users
  async editSystemUser(
    { commit },
    {
      token,
      email,
      password,
      firstname,
      lastname,
      middlename,
      mobile,
      role_id,
      position_id,
      status,
      id
    }
  ) {
    console.log(lastname);
    return await axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/users/admin/edit/user/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        email,
        password,
        firstname,
        lastname,
        middlename,
        mobile,
        role_id,
        position_id,
        status,
        id
      }
    }).then(res => {
      commit("EDIT_SYSUSER", res);
      return res;
    });
  },

  async emailNewUser({ commit }, { link, id, token }) {
    console.log("send email", link);
    return await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/email/applicant/${id}/user-credentials`,
      headers: {
        Authorization: `Bearer ${token}`
      },
      data: {
        link
      }
    }).then(res => {
      commit("EMAIL_CRED", res);
      return res;
    });
  },

  async getPositionDetail({ commit }, { id, token }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/positions/view/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    }).then(res => {
      commit("POSITION_DET", res.data.view);
      return res.data.view[0];
    });
  }
};

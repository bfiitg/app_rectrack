export default {
  ADD_DEPT(state, data) {
    state.departmentsState = data;
  },
  EDIT_DEPT(state, data) {
    state.departmentsState = data;
  },
  VIEW_DEPT(state, data) {
    state.allDepartmentsState = data;
  },
  VIEW_TEAM(state, data) {
    state.allTeamsState = data;
  },
  EDIT_TEAM(state, data) {
    state.allTeamsState = data;
  },
  VIEW_POSITION(state, data) {
    state.positionsState = data;
  },
  ADD_POS(state, data) {
    state.positionsState = data;
  },
  EDIT_POS(state, data) {
    state.positionsState = data;
  },
  VIEW_SYSUSER(state, data) {
    state.systemUsersState = data;
  },
  VIEW_SYSUSERDET(state, data) {
    state.systemUsersState = data;
  },
  ADD_SYSUSER(state, data) {
    state.systemUsersState = data;
  },
  EDIT_SYSUSER(state, data) {
    state.systemUsersState = data;
  },
  EMAIL_CRED(state, data) {
    state.systemUsersState = data;
  },
  POSITION_DET(state, data) {
    state.positionsState = data;
  }
};
